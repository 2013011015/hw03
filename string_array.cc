// string_array.cc

using namespace std;
#include "string_array.h"
StringArray::StringArray()
{
  	strings_=NULL;
	size_=0;
	alloc_=0;
}
StringArray::StringArray(const StringArray& str_array)
{
	size_=str_array.size();
	alloc_=size_;
	Copy(str_array);
}
int StringArray::Find(const std::string& str)const
{
	int i;
	for (i==0;i<size_;i++)
	{
		if (strings_[i]==str)
			return i;
		else
			return -1;
	}
}
std::string StringArray:: Concat() const
{
	std::string Last;
	for (int i=0;i<size_;i++)
	{
		Last=Last+strings_[i];
		if (i!=size_-1)
			Last=Last+" ";
	}
	return Last;
}
void StringArray::Add(const std::string& str)
{
	Resize(size_+1);
	strings_[size_-1]=str;
}
void StringArray::Erase(size_t i)
{
	for (int j=i;j<size_-1;j++)
		strings_[j]=strings_[j+1];
        size_--;
}
void StringArray::Set(size_t i,const std::string& str)
{
	if (size_-1<i)
		Resize(i+1);
	strings_[i]=str;
}
void StringArray::Copy(const StringArray& str_array)
{
	strings_=new string[str_array.size()];
	for (int i=0;i<size_;i++)
		strings_[i]=str_array.strings_[i];
}
void StringArray::Resize(size_t new_size)
{
	if (new_size>alloc_)
	{
		string* emp;
		emp=new string[size_];
		for(int i=0;i<size_;i++)
			emp[i]=strings_[i];
		delete[] strings_;
		strings_=new string[new_size];
		for(int i=0;i<size_;i++)
			strings_[i]=emp[i];
		alloc_=new_size;
		size_=new_size;
	}
	else size_=new_size;
}
