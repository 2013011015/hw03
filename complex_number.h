// complex_number.h

#ifndef _COMPLEX_NUMBER_H_
#define _COMPLEX_NUMBER_H_

class ComplexNumber {
 public:
  ComplexNumber()
  {
	real_=0;
	imag_=0;
  }
  ComplexNumber(const ComplexNumber& c)
  {
	real_=c.real_;
	imag_=c.imag_;
  }
	
  ComplexNumber(double real, double imag)
  {
	real_=real;
	imag_=imag;
  }

  double real() const{return real_;}
  double imag() const{return imag_;}
  void Set(double real, double imag);
  void Copy(const ComplexNumber& c);

  ComplexNumber Add(const ComplexNumber& c) const;
  ComplexNumber Subtract(const ComplexNumber& c) const;
  ComplexNumber Multiply(const ComplexNumber& c) const;
  ComplexNumber Divide(const ComplexNumber& c) const;

 private:
  double real_, imag_;
};

#endif  // _COMPLEX_NUMBER_H_
