// complex_number.cc

#include "complex_number.h"
void ComplexNumber::Set(double real,double imag)
{
	real_=real;
	imag_=imag;
}
void ComplexNumber::Copy(const ComplexNumber& c)
{
	real_=c.real_;
	imag_=c.imag_;
}
ComplexNumber ComplexNumber::Add(const ComplexNumber& c)const
{
	ComplexNumber a;
	double real1,imag1;
	real1=real_+c.real_;
	imag1=imag_+c.imag_;
	a.Set(real1,imag1);
	return a;
}
ComplexNumber ComplexNumber::Subtract(const ComplexNumber& c)const
{
	ComplexNumber a;
	double real1,imag1;
	real1=real_-c.real_;
	imag1=imag_-c.imag_;
	a.Set(real1,imag1);
	return a;
}
ComplexNumber ComplexNumber::Multiply(const ComplexNumber& c)const
{
	ComplexNumber a;
	double real1,imag1;
	real1=(real_*c.real_)-(imag_*c.imag_);
	imag1=(imag_*c.real_)+(real_*c.imag_);
	a.Set(real1,imag1);
	return a;
}
ComplexNumber ComplexNumber::Divide(const ComplexNumber& c)const
{
	ComplexNumber a;
	double real1,imag1;
	real1=((real_*c.real_)+(imag_*c.imag_))/((c.real_*c.real_)+(c.imag_*c.imag_));
	imag1=((imag_*c.real_)-(real_*c.imag_))/((c.real_*c.real_)+(c.imag_*c.imag_));
	a.Set(real1,imag1);
	return a;
}
